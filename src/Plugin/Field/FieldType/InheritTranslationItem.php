<?php

namespace Drupal\translation_inheritance\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'translation_inheritance' entity field type.
 *
 * @FieldType(
 *   id = "translation_inheritance",
 *   label = @Translation("Translation Inheritance"),
 *   description = @Translation("Translation Inheritance"),
 *   no_ui = TRUE,
 *   cardinality = 1,
 *   default_widget = "translation_inheritance_widget"
 * )
 */
class InheritTranslationItem extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'source_language' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['source_language'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Source language'))
      ->setSetting('case_sensitive', TRUE)
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('source_language')->getValue();
    return $value === NULL || $value === '';
  }

}
