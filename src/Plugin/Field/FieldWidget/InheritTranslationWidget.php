<?php

namespace Drupal\translation_inheritance\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Plugin implementation of the 'translation_inheritance_widget' widget.
 *
 * @FieldWidget(
 *   id = "translation_inheritance_widget",
 *   label = @Translation("Translation Inheritance"),
 *   field_types = {
 *     "translation_inheritance"
 *   }
 * )
 */
class InheritTranslationWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();
    if ($entity->isDefaultTranslation()) {
      return [];
    }

    $current_langcode = $entity->language()->getId();
    $languages = $entity->getTranslationLanguages();
    if (isset($languages[$current_langcode])) {
      unset($languages[$current_langcode]);
    }

    $element['source_language'] = $element + [
      '#type' => 'select',
      '#default_value' => $items[$delta]->source_language ?? NULL,
      '#empty_option' => $this->t("Don't inherit translation"),
      '#options' => array_map(function (LanguageInterface $language) {
        return $language->getName();
      }, $languages),
    ];

    return $element;
  }

}
