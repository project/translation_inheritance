<?php

namespace Drupal\translation_inheritance;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines some helper functions and callbacks for translation inheritance.
 */
class TranslationInheritance implements TranslationInheritanceInterface, TrustedCallbackInterface {

  /**
   * Retrieves the correct translation for this entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The input entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|bool
   *   The output translation, or FALSE if some conditions didn't apply.
   */
  public static function getCorrectTranslation(ContentEntityInterface $entity) {
    if (!$entity->hasField(TranslationInheritanceInterface::FIELD_NAME)) {
      return FALSE;
    }

    // If this is the default translation, or this translation simply doesn't
    // inherit from any other language, we instantly return.
    if (!$entity->get(TranslationInheritanceInterface::FIELD_NAME)->source_language) {
      return FALSE;
    }

    // Translation inheritance can be recursive, make sure to get the actual
    // entity we need to render.
    while ($source_language = $entity->get(TranslationInheritanceInterface::FIELD_NAME)->source_language) {
      $entity = $entity->getTranslation($source_language);
    }

    return $entity;
  }

  /**
   * Retrieves the entity from the request, if possible and present.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The current request object (optional).
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|bool
   *   The content entity, or false if not applicable.
   */
  public static function getEntityFromRequest(?Request $request = NULL) {
    $request = $request ?: \Drupal::request();
    $route = $request->attributes->get('_route');
    if (preg_match('/entity.([a-z\_]+).canonical/', $route, $matches)) {
      $entity_type_id = $matches[1];
    }
    elseif (!$entity_type_id = $request->attributes->get('entity_type_id')) {
      return FALSE;
    }

    $entity = $request->attributes->get($entity_type_id);
    if (!$entity instanceof ContentEntityInterface) {
      return FALSE;
    }

    return $entity;
  }

  /**
   * Shows a message on the layout builder page if the translation is inherited.
   */
  public static function alterLayoutBuilder($element) {
    $request = \Drupal::request();
    if (!$entity = static::getEntityFromRequest($request)) {
      return $element;
    }

    if (!$entity = static::getCorrectTranslation($entity)) {
      return $element;
    }

    $language = $entity->language();
    $url = Url::createFromRequest($request)->setOption('language', $language);
    \Drupal::messenger()->addWarning(t('This layout will not be shown because this translation inherits its content from the @source-language.
To edit that content, please <a href=":link">click here</a>.', [
  '@source' => $language->getId(),
  ':link' => $url->toString(),
]));

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['alterLayoutBuilder'];
  }

}
