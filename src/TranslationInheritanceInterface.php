<?php

namespace Drupal\translation_inheritance;

/**
 * Defines constants, properties and methods for TranslationInheritance.
 */
interface TranslationInheritanceInterface {

  const FIELD_NAME = 'translation_inheritance__inherit';

}
