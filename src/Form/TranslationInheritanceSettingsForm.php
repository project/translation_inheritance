<?php

namespace Drupal\translation_inheritance\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\translation_inheritance\TranslationInheritanceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the settings form for the Translation Inheritance-module.
 */
class TranslationInheritanceSettingsForm extends ConfigFormBase {

  /**
   * Constructs the Translation Inheritance settings form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo
  ) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['translation_inheritance.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'translation_inheritance_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('translation_inheritance.settings');
    $form['#tree'] = TRUE;

    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface[] $entity_types */
    $entity_types = array_filter($this->entityTypeManager->getDefinitions(), function ($entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface && $entity_type->isTranslatable();
    });

    foreach ($entity_types as $id => $entity_type) {
      $bundles = array_map(function ($bundle) {
        return $bundle['label'];
      }, array_filter($this->entityTypeBundleInfo->getBundleInfo($id), function ($bundle) {
        return !empty($bundle['translatable']);
      }));

      $enabled = $config->get('entity_types.' . $id . '.enabled') ?: FALSE;
      $form[$id] = [
        '#type' => 'details',
        '#title' => $entity_type->getLabel(),
        '#open' => $enabled,
      ];

      $form[$id]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable translation inheritance for this entity type'),
        '#default_value' => $enabled,
        '#disabled' => empty($bundles),
      ];

      if (empty($bundles)) {
        $form[$id]['no_bundles'] = [
          '#markup' => $this->t('This entity type has no translatable bundles. You can change this in the <a href=":link" target="_blank">content translation settings</a>.', [
            ':link' => Url::fromRoute('language.content_settings_page')->toString(),
          ]),
        ];
      }
      else {
        $form[$id]['bundles'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Bundles'),
          '#states' => [
            'visible' => [
              ':input[name="' . $id . '[enabled]"]' => ['checked' => TRUE],
            ],
          ],
        ];

        foreach ($bundles as $bundle => $bundle_label) {
          $form[$id]['bundles'][$bundle] = [
            '#type' => 'checkbox',
            '#title' => $bundle_label,
            '#default_value' => $config->get('entity_types.' . $id . '.bundles.' . $bundle) ?: FALSE,
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface[] $entity_types */
    $entity_types = array_filter($this->entityTypeManager->getDefinitions(), function ($entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface && $entity_type->isTranslatable();
    });

    $config = $this->config('translation_inheritance.settings');
    $new_values = array_intersect_key($form_state->getValues(), $entity_types);

    // Check if any fields need to be added or removed.
    foreach ($entity_types as $id => $entity_type) {
      $bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo($id));

      if (!empty($new_values[$id]['enabled'])) {
        foreach (($new_values[$id]['bundles'] ?? []) as $bundle => $value) {
          if (!empty($value)) {
            $this->addTranslationInheritanceField($id, $bundle, TranslationInheritanceInterface::FIELD_NAME);
          }
          else {
            // Delete the field from the bundle, if it exists.
            if ($field = FieldConfig::loadByName($id, $bundle, TranslationInheritanceInterface::FIELD_NAME)) {
              $field->delete();
            }
          }
        }
      }

      // Delete fields for entity types that are no longer supported.
      if ($config->get('entity_types.' . $id . '.enabled') && empty($new_values[$id]['enabled'])) {
        // Delete all the bundle fields.
        foreach ($bundles as $bundle) {
          if ($field = FieldConfig::loadByName($id, $bundle, TranslationInheritanceInterface::FIELD_NAME)) {
            $field->delete();
          }
        }

        // Finally, delete the field storage.
        if ($field_storage = FieldStorageConfig::loadByName($id, TranslationInheritanceInterface::FIELD_NAME)) {
          $field_storage->delete();
        }
      }
    }

    $config
      ->set('entity_types', $new_values)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Adds a translation inheritance field to a given bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The bundle.
   * @param string $field_name
   *   The name for the layout section field.
   *
   * @return void
   *   Does not return a value.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function addTranslationInheritanceField($entity_type_id, $bundle, $field_name): void {
    $field = FieldConfig::loadByName($entity_type_id, $bundle, $field_name);
    if (!$field) {
      $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
      if (!$field_storage) {
        $field_storage = FieldStorageConfig::create([
          'entity_type' => $entity_type_id,
          'field_name' => $field_name,
          'type' => 'translation_inheritance',
          'locked' => TRUE,
        ]);
        $field_storage->setTranslatable(TRUE);
        $field_storage->save();
      }

      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $bundle,
        'label' => t('Inherit translation from'),
        'description' => t('Inheriting values from another translation will not copy the values into the translation, but rather render the source language content on the newly translated item.'),
      ]);
      $field->setTranslatable(TRUE);
      $field->save();
    }
  }

}
